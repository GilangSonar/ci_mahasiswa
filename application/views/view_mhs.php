<!DOCTYPE html>
<html>
<head>
    <title><?= $title;?></title>
    <link rel="stylesheet" href="<?= base_url('css/bootstrap.min.css')?>"/>

</head>
<body style="padding-top: 40px">

<div class="container">

    <div class="page-header">
        <h1 class="text-center">
            Input Mahasiswa Dengan Kode Otomatis ( Codeigniter & MySQL )
        </h1>
    </div>
    <div class="row">
        <div class="col-md-4 alert alert-info">
            <form role="form" action="<?= site_url('mahasiswa/insert')?>" method="post">
                <div class="form-group">
                    <label>Kode Mahasiswa</label>
                    <input type="text" class="form-control" value="<?= $kode ?>" name="kd_mhs" readonly required="">
                </div>
                <div class="form-group">
                    <label>Nama Mahasiswa</label>
                    <input type="text" class="form-control" name="nama" required="">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" required="">
                </div>

                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </form>
        </div>

        <div class="col-md-8">
            <table class="table table-hover table-bordered ">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Mahasiswa</th>
                    <th>Nama</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; if(isset($dt_mhs )){ foreach($dt_mhs as $row) { ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $row->kd_mhs?></td>
                        <td><?= $row->nama?></td>
                        <td><?= $row->email?></td>
                    </tr>
                <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>