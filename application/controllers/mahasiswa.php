<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 5/4/14
 * Time: 4:46 PM
 */
class Mahasiswa extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('model_mhs');
    }

    function index(){
        $data=array(
            'title'=>'Mahasiswa',
            'dt_mhs'=>$this->model_mhs->getData(),
            'kode'=>$this->model_mhs->getKodeMahasiswa(),
        );
        $this->load->view('view_mhs',$data);
    }

    function insert(){
        $data=array(
            'kd_mhs'=>$this->input->post('kd_mhs'),
            'nama'=>$this->input->post('nama'),
            'email'=>$this->input->post('email'),
        );
        $this->model_mhs->inputData($data);
        redirect('');
    }
}