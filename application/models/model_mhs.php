<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 5/4/14
 * Time: 4:49 PM
 */

class Model_mhs extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getKodeMahasiswa(){
        $q = $this->db->query("select MAX(RIGHT(kd_mhs,2)) as code_max from mhs");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%02s", $tmp);
            }
        }else{
            $code = "01";
        }
        return "MHS/2014/".$code;
    }

    function getData(){
        $query = $this->db->query('select * from mhs');
        return $query->result();
    }
    function inputData($data){
        $query = $this->db->insert('mhs',$data);
        return $query;
    }
}

